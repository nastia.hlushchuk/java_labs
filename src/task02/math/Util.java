package ua.khpi.hlushchuk_anastasia.task02.math;

/**
 * Math class
 *
 * @author Hlushchuk
 *
 */
public abstract class Util {

    /**
     * Get random double between range
     *
     * @param min Min
     * @param max Max
     * @return generated value
     */
    public static double getRandomDoubleBetweenRange(double min, double max) {
        return Math.random() * (max - min) + min;
    }
}
