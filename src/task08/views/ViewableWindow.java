package ua.khpi.hlushchuk_anastasia.task08.views;

import ua.khpi.hlushchuk_anastasia.task06.views.View;
import ua.khpi.hlushchuk_anastasia.task06.views.Viewable;

/**
 * Fabricate object
 *
 * @author Hlushchuk
 *
 */
public class ViewableWindow implements Viewable {
    /**
     * @return fabricated object
     */
    @Override
    public View getView() {
        return new ViewWindow();
    }
}
