package ua.khpi.hlushchuk_anastasia.task06.views;

import ua.khpi.hlushchuk_anastasia.task06.views.View;

public class ViewableTable {
    /**
     * Fabricate table view
     * @return fabricated object
     */
    public View getView(){
        return new ViewTable();
    }
}
