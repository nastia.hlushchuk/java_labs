package ua.khpi.hlushchuk_anastasia.task06.views;

/**
 * fabricating object
 * @author Hlushchuk
 *
 */
public class ViewableResult implements Viewable{
    /**
     * Fabricate view
     * @return fabricated object
     */
    public View getView(){
        return new ViewResult();
    }
}
