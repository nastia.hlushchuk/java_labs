package ua.khpi.hlushchuk_anastasia.task06.menu.commands;

import ua.khpi.hlushchuk_anastasia.task06.views.View;
import ua.khpi.hlushchuk_anastasia.task05.menu.commands.ConsoleCommand;

/**
 * Show information command
 *
 * @author Hlushchuk
 *
 */
public class ShowInformationCommand implements ConsoleCommand {

    private int menuKey;
    private View view;

    public ShowInformationCommand(int key,View view) {
        this.menuKey=key;
        this.view=view;
    }
    @Override
    public void execute() {
        this.view.show();
    }

    @Override
    public int getKey() {
        return menuKey;
    }

    @Override
    public String toString() {
        return "Show information.";
    }
}
