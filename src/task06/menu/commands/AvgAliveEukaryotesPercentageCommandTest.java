package ua.khpi.hlushchuk_anastasia.task06.menu.commands;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import ua.khpi.hlushchuk_anastasia.task02.FileBasedRepository;
import ua.khpi.hlushchuk_anastasia.task02.Repository;
import ua.khpi.hlushchuk_anastasia.task02.biology.FissionSimulation;
import ua.khpi.hlushchuk_anastasia.task06.views.ViewResult;
import ua.khpi.hlushchuk_anastasia.task02.biology.FissionSimulation.FissionResult;

class AvgAliveEukaryotesPercentageCommandTest {

    @Test
    void getResult_AvgAliveEukaryotesPercentage_AffterInitView() throws ClassNotFoundException, IOException {
        String fileName="task06.menu.commands.avg.ini";
        Repository repository=new FileBasedRepository(fileName);
        ViewResult view=new ViewResult(repository,1);
        view.init();
        view.save();

        @SuppressWarnings("unchecked")
        ArrayList<FissionSimulation.FissionResult> fissionResults=(ArrayList<FissionSimulation.FissionResult>)repository.load();
        assertNotNull(fissionResults);

        double expectedAvg=0;
        for (FissionResult fissionResult : fissionResults) {
            expectedAvg+=fissionResult.getAliveEukaryotesPercentage();
        }
        expectedAvg/=fissionResults.size();

        AvgAliveEukaryotesPercentageCommand command=new AvgAliveEukaryotesPercentageCommand(view);
        command.execute();
        double actualAvg=command.getResult();

        assertEquals(expectedAvg, actualAvg);
    }

}
