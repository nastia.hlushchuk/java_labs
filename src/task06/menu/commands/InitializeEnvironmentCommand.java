package ua.khpi.hlushchuk_anastasia.task06.menu.commands;

import ua.khpi.hlushchuk_anastasia.task06.views.View;
import ua.khpi.hlushchuk_anastasia.task05.menu.commands.ConsoleCommand;

/**
 * Initialize environment command
 *
 * @author Hlushchuk
 *
 */
public class InitializeEnvironmentCommand implements ConsoleCommand {

    private int menuKey;
    private View view;

    public InitializeEnvironmentCommand(int key,View view) {
        this.menuKey=key;
        this.view=view;
    }
    @Override
    public void execute() {
        System.out.println("[Initialize environment] Process....");
        this.view.init();
        System.out.println("[Initialize environment] Done.");
    }

    @Override
    public int getKey() {
        return menuKey;
    }

    @Override
    public String toString() {
        return "Initialize environment.";
    }

}
