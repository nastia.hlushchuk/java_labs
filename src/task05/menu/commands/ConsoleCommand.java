package ua.khpi.hlushchuk_anastasia.task05.menu.commands;


/**
 * Console Command
 *
 * @author Hlushchuk
 *
 */
public interface ConsoleCommand extends Command {
    /**
     *
     * @return command key
     */
    public int getKey();
}
