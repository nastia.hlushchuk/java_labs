package ua.khpi.hlushchuk_anastasia.task05.menu.commands;

/**
 * Interface of command
 *
 * @author Hlushchuk
 *
 */
public interface Command {
    /**
     * Command activity
     */
    public void execute();
}
