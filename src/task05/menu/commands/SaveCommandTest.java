package ua.khpi.hlushchuk_anastasia.task05.menu.commands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ua.khpi.hlushchuk_anastasia.task03.views.ViewResult;

class SaveCommandTest {

    @Test
    void getKey_Get5_InitWithParameter5() {
        int expectKey=5;
        ViewResult viewResult=new ViewResult();
        SaveCommand command=new SaveCommand(expectKey, viewResult);

        int actualKey= command.getKey();
        assertEquals(expectKey, actualKey);
    }

    @Test
    void toString_GetCommandName() {
        String expectString="Save.";
        ViewResult viewResult=new ViewResult();
        SaveCommand command=new SaveCommand(1, viewResult);

        String actualString= command.toString();
        assertEquals(expectString, actualString);
    }

}
