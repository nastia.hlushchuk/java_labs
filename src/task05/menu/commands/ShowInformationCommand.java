package ua.khpi.hlushchuk_anastasia.task05.menu.commands;

import ua.khpi.hlushchuk_anastasia.task03.views.View;

/**
 * Show information command
 *
 * @author Hlushchuk
 *
 */
public class ShowInformationCommand implements ConsoleCommand {

    private int menuKey;
    private View view;

    /**
     * Initializes the ShowInformationCommand
     *
     * @param key  Menu key
     * @param view view object
     */
    public ShowInformationCommand(int key, View view) {
        this.menuKey = key;
        this.view = view;
    }

    /**
     * Command activity
     */
    @Override
    public void execute() {
        this.view.show();
    }

    /**
     *
     * @return command key
     */
    @Override
    public int getKey() {
        return menuKey;
    }

    @Override
    public String toString() {
        return "Show information.";
    }
}
