package ua.khpi.hlushchuk_anastasia.task05;

import ua.khpi.hlushchuk_anastasia.task03.views.View;
import ua.khpi.hlushchuk_anastasia.task05.menu.Menu;
import ua.khpi.hlushchuk_anastasia.task05.menu.commands.InitializeEnvironmentCommand;
import ua.khpi.hlushchuk_anastasia.task05.menu.commands.LoadCommand;
import ua.khpi.hlushchuk_anastasia.task05.menu.commands.SaveCommand;
import ua.khpi.hlushchuk_anastasia.task05.menu.commands.ShowInformationCommand;

/**
 * Application class
 *
 * @author Hlushchuk
 *
 */
public class Application {
    private static Application instance = new Application();

    /**
     * private default constructor
     */
    private Application() {
    }

    /**
     * method for getting single object
     *
     * @return Application
     */
    public static Application getInstance() {
        return instance;
    }

    // private View view = new ViewableTable().getView();
    private Menu menu = new Menu();

    /**
     * create and run Menu
     */
    public void run(View view) {
        menu.clear();
        menu.add(new InitializeEnvironmentCommand(1, view));
        menu.add(new ShowInformationCommand(2, view));
        menu.add(new SaveCommand(3, view));
        menu.add(new LoadCommand(4, view));
        menu.execute();
    }
}
