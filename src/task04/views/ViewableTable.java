package ua.khpi.hlushchuk_anastasia.task04.views;

import ua.khpi.hlushchuk_anastasia.task03.views.View;
import ua.khpi.hlushchuk_anastasia.task03.views.Viewable;

/**
 * fabricating object
 *
 * @author Hlushchuk
 *
 */
public class ViewableTable implements Viewable {
    /**
     * fabricate table view
     *
     * @return fabricated object
     */
    @Override
    public View getView() {
        return new ViewTable();
    }
}
