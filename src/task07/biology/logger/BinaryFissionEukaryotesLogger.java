package ua.khpi.hlushchuk_anastasia.task07.biology.logger;

import ua.khpi.hlushchuk_anastasia.task07.AnnotatedObserver;
import ua.khpi.hlushchuk_anastasia.task07.Event;
import ua.khpi.hlushchuk_anastasia.task07.biology.Environment;

/*
 * Binary fission eukaryotes logger
* @author Hlushchuk
*/
public class BinaryFissionEukaryotesLogger extends AnnotatedObserver {

    @Event(Environment.ON_BINARY_FISSION_EUKARYOTES)
    public void onBinaryFissionEukaryotesLogger(Environment environment) {
        System.out.println("[BinaryFissionEukaryotesLogger] Eukaryote quantity before binary fission eukaryotes : "+environment.getEukaryoteQuantity());
    }
}
