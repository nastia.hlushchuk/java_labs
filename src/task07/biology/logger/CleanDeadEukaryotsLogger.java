package ua.khpi.hlushchuk_anastasia.task07.biology.logger;

import ua.khpi.hlushchuk_anastasia.task07.AnnotatedObserver;
import ua.khpi.hlushchuk_anastasia.task07.Event;
import ua.khpi.hlushchuk_anastasia.task07.biology.Environment;

/*
 * Clean dead eukaryots logger
* @author Hlushchuk
*/
public class CleanDeadEukaryotsLogger extends AnnotatedObserver {

    @Event(Environment.ON_CLEANING_DEAD_EUKARYOTS)
    public void onCleaningDeadEukaryots(Environment environment) {
        System.out.println("[CleanDeadEukaryotsLogger] Eukaryote quantity before clean dead eukaryote : "+environment.getEukaryoteQuantity());
    }

    @Event(Environment.ON_CLEANED_DEAD_EUKARYOTS)
    public void onCleanedDeadEukaryots(Environment environment) {
        System.out.println("[CleanDeadEukaryotsLogger] Eukaryote quantity after clean dead eukaryote : "+environment.getEukaryoteQuantity());
    }
}
