package ua.khpi.hlushchuk_anastasia.task07;

import java.lang.annotation.*;

/**
 * Runtime annotation for appointment to Observer methods
 *
 * @author Hlushchuk
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
    public String value();
}
