package ua.khpi.hlushchuk_anastasia.task07;

/**
 * has method for observer and observable interaction
 *
 * @author Hlushchuk
 *
 */
public interface Observer {
    /**
     * called by observable object for every observer
     *
     * @param observable
     *            link to observable object
     * @param event
     *            information about event
     */
    public void HandleEvent(Observable observable, Object event);
}
