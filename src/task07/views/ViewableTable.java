package ua.khpi.hlushchuk_anastasia.task07.views;

import ua.khpi.hlushchuk_anastasia.task07.views.View;

public class ViewableTable {
    /**
     * fabricate table view
     * @return fabricated object
     */
    public View getView(){
        return new ViewTable();
    }
}
