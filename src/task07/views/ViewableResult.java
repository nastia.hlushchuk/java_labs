package ua.khpi.hlushchuk_anastasia.task07.views;

/**
 * fabricating object
 * @author Hlushchuk
 *
 */
public class ViewableResult implements Viewable{
    /**
     * fabricate view
     * @return fabricated object
     */
    public View getView(){
        return new ViewResult();
    }
}
