package ua.khpi.hlushchuk_anastasia.task07.views;

/**
 * fabricating object interface
 * @author Hlushchuk
 *
 */
public interface Viewable {
    /**
     * fabricate view
     * @return fabricated object
     */
    View getView();
}
