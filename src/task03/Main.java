package ua.khpi.hlushchuk_anastasia.task03;

import ua.khpi.hlushchuk_anastasia.task03.menu.Menu;
import ua.khpi.hlushchuk_anastasia.task03.views.ViewableResult;

public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu(new ViewableResult());
        menu.dialog();
    }
}
