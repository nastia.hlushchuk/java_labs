package ua.khpi.hlushchuk_anastasia.task03.views;

/**
 * fabricating object interface
 *
 * @author Hlushchuk
 *
 */
public interface Viewable {
    /**
     * fabricate view
     *
     * @return fabricated object
     */
    View getView();
}
