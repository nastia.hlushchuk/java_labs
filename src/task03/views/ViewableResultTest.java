package ua.khpi.hlushchuk_anastasia.task03.views;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ViewableResultTest {

    @Test
    void getView_ViewResultObject() {
        ViewableResult viewableResult=new ViewableResult();
        View view=viewableResult.getView();

        assertTrue(view instanceof  ViewResult);
    }

}
